from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):
    birthdate = models.DateTimeField(null=True) #default is that it can't be null. 
    #Make changes like this in the code and the migrate it to keep it in sync

    
