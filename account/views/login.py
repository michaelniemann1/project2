from django.conf import settings
from django_mako_plus import view_function, jscontext
from datetime import datetime, timezone
from django import forms
from account.forms import loginForm
from account import models as amod
from django.contrib.auth import authenticate, login, logout 
from django.core.exceptions import ValidationError 
from django.shortcuts import render, redirect 
from django.http import HttpResponseRedirect


@view_function
def process_request(request):

    if request.method == 'POST':
        form = loginForm(request.POST)
        if form.is_valid():
          user = authenticate(username=request.POST['username'], password=request.POST['password'])
          login(request, user)
          return HttpResponseRedirect("/homepage/index", user)
        else:
            forms.ValidationError("Wrongo")
    else:
        form = loginForm()

    context = {
        'form': form
    }

    return request.dmp.render('login.html', context)

# from django.conf import settings
# from django_mako_plus import view_function, jscontext
# from django import forms
# from account import models as amod
# from django.shortcuts import render, redirect
# from django.contrib.auth import authenticate, login, logout

# from django.core.exceptions import ValidationError
# from django.http import HttpResponseRedirect
# # from django.http import HttpResponseRedirect




# @view_function
# def process_request(request):
#    #When the user hits the "Submit"
#    if request.method == 'POST':
#        form = LoginForm(request.POST)
#        # Check if the form is valid. Everything after the form.is_valid is VALID and everything is good, don't need checks
#        if form.is_valid():
#            # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
#            username = form.cleaned_data.get('username')
#            password = form.cleaned_data.get('password')
#            user = authenticate(username=username, password=password)
#            if user is not None:
#                login(request, user)
#                # Redirect to a success page.
#                return redirect('/homepage/index/', user)
#    ##The following code will execute if it is a GET Method (default blank form)
#    else:
#         form = LoginForm()
#         context = {
#             'form': form
#         }
#    return request.dmp.render('login.html', context)

#    class LoginForm(forms.Form):
#    username = forms.CharField()
#    password = forms.CharField(widget=forms.PasswordInput)
#    def clean(self, *args, **kwargs):
#        username = self.cleaned_data.get('username')
#        password = self.cleaned_data.get('password')
#        ##The forms.ValidationError has to be done BEFORE process_request form.is_valid
#        if username and password:
#            user = authenticate(username=username, password=password)
#            if not user:
#                raise forms.ValidationError('User does not exist')
#            if not user.check_password(password):
#                raise forms.ValidationError('Password is incorrect')
#            if not user.is_active:
#                raise forms.ValidationError('User is not active')
#        return super(LoginForm, self).clean(*args, **kwargs)





# from django.conf import settings
# from django_mako_plus import view_function, jscontext

# from django import forms
# from account import models as amod
# from django.core.exceptions import ValidationError
# from django.utils.translation import ugettext_lazy as _

# from django.contrib.auth import authenticate, login, logout
# from django.http import HttpResponseRedirect

# class MyForm(forms.Form):
#     '''an example form '''
#     username = forms.CharField(label="Username")
#     password = forms.CharField(label='Password', widget=forms.PasswordInput())


#     def clean(self):
#         user = authenticate(username=self.ceaned_data.get('username'), password=self.cleaned_data.get('password'))
#         if user is None:
#             raise forms.ValidationError('Invalid username or password.')
#         return self.cleaned_data



# @view_function
# def process_request(request):
#     #form = 'something'

#     # If this is a POST request then process the Form data
#     if request.method == 'POST':
#         form = MyForm(request.POST)
#         if form.is_valid():
#                 user = authenticate(username=form.cleaned_data.get('username'), password=form.cleaned_data.get('password'))
#                 login(request, user)
#                 return HttpResponseRedirect('/')
#     else: #GET
#         form = MyForm()
#         context = {
#             'form': form,
#         }
#         #render the template
#         return request.dmp.render('login.html', context)






#         # Create a form instance and populate it with data from the request (binding):
#         form = CheclLogin(request.POST)

#         # Check if the form is valid:
#         if form.is_valid():
#             # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
#             book_instance.due_back = form.cleaned_data['renewal_date']
#             book_instance.save()

#             # redirect to a new URL:
#             return HttpResponseRedirect(reverse('all-borrowed') )

#     # If this is a GET (or any other method) create the default form.
#     else: #GET
#         proposed_renewal_date = datetime.date.today() + datetime.timedelta(weeks=3)
#         form = RenewBookForm(initial={'renewal_date': proposed_renewal_date})

    
#     class CheckLogin(forms.Form):
#     renewal_date = forms.DateField(help_text="Enter a date between now and 4 weeks (default 3).")

#     def clean_renewal_date(self):
#         data = self.cleaned_data['renewal_date']
        
#         # Check if a date is not in the past. 
#         if data < datetime.date.today():
#             raise ValidationError(_('Invalid date - renewal in past'))

#         # Check if a date is in the allowed range (+4 weeks from today).
#         if data > datetime.date.today() + datetime.timedelta(weeks=4):
#             raise ValidationError(_('Invalid date - renewal more than 4 weeks ahead'))

#         # Remember to always return the cleaned data.
#         return data

#     context = {
#         'form' : form,
#     }
#     return request.dmp.render('login.html', context)

# class LoginForm(forms.form):
#     if request.method == 'POST':
#         #create a form instance and populate it with data from the request

    


# class RenewBookForm(forms.Form):
#     renewal_date = forms.DateField(help_text="Enter a date between now and 4 weeks (default 3).")

#     def clean_renewal_date(self):
#         data = self.cleaned_data['renewal_date']
        
#         # Check if a date is not in the past. 
#         if data < datetime.date.today():
#             raise ValidationError(_('Invalid date - renewal in past'))

#         # Check if a date is in the allowed range (+4 weeks from today).
#         if data > datetime.date.today() + datetime.timedelta(weeks=4):
#             raise ValidationError(_('Invalid date - renewal more than 4 weeks ahead'))

#         # Remember to always return the cleaned data.
#         return data


# def renew_book_librarian(request, pk):
#     book_instance = get_object_or_404(BookInstance, pk=pk)

#     # If this is a POST request then process the Form data
#     if request.method == 'POST':

#         # Create a form instance and populate it with data from the request (binding):
#         form = RenewBookForm(request.POST)

#         # Check if the form is valid:
#         if form.is_valid():
#             # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
#             book_instance.due_back = form.cleaned_data['renewal_date']
#             book_instance.save()

#             # redirect to a new URL:
#             return HttpResponseRedirect(reverse('all-borrowed') )

#     # If this is a GET (or any other method) create the default form.
#     else:
#         proposed_renewal_date = datetime.date.today() + datetime.timedelta(weeks=3)
#         form = RenewBookForm(initial={'renewal_date': proposed_renewal_date})

#     context = {
#         'form': form,
#         'book_instance': book_instance,
#     }

#     return render(request, 'catalog/book_renew_librarian.html', context)