from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django_mako_plus import view_function

@view_function
def process_request(request):
    # log the user out
    logout(request)
    #
    return HttpResponseRedirect('/homepage/index') #put in whatever html page it goes to for the login page (restarting the browser)
    