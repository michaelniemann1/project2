from django import forms 
from django.contrib.auth.models import User
import django.contrib.auth
from django.contrib.auth import authenticate, login, logout 
from django.core.exceptions import ValidationError 

class loginForm(forms.Form):
    Username = forms.CharField()
    Password = forms.CharField(max_length=32, widget=forms.PasswordInput)

    def clean(self):
        #data = self.cleaned_data['Password']
        user = authenticate(username=self.cleaned_data['username'], password=self.cleaned_data['password'])
        if user is None:
            #ValueError("Username or password is incorrect")
            raise forms.ValidationError('Invalid Username or Password.')
        return self.cleaned_data