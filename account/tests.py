from django.test import TestCase
from account import models as amod
from datetime import datetime


# Create your tests here.
class AccountTests(TestCase):
    fixtures = [ 'account.yaml']
    def setUp(self):
        self.homer = amod.User()
        self.homer.username = "homer2"
        self.homer.set_password('doh!')
        self.homer.first_name = "Homer"
        self.homer.first_name = "Simpson"
        self.homer.birthdate = datetime(2000,1 ,1)
        self.homer.save()
    # four letters 'test' are reserverd for all test cases
    def test_user_login(self):
        credentials = {
            'username': 'homer2',
            'password': 'doh!'
        }
        #response = self.client.post('/account/login/', credentials)
        response = self.client.get('/account/login/', credentials)
        #get the request object (testing framework embeds it as response.wsgi_request)
        request = response.wsgi_request
        #this next line is ONLY for debugging the test, it should be removed after things work
        #self.print_html(response.content)
        #if it worked, the request.user will be the homer object and is_authenticated will be true
        self.assertTrue(request.user.is_authenticated, msg="User should have authenticated")
        self.assertEqual(request.user.id, self.homer.id, msg="User should be been homer")
        #ift worked, the respnse should be a redirect code (login.py returned HttpResonseRedirect)

        self.assertEqual(response.status_code, 302, msg="User wasn't redirected")

        #logout
        response = self.client.get('/account/logout/')
        request = response.wsgi_request
        self.assertEqual(response.status_code, 302, msg="User wasn't redirected")
        self.assertFalse(response.user.is_authenticated, msg="User should not have been authenticated")


    def test_user_get(self): 
        u1 = amod.User.objects.get(id=1)
        self.assertEqual(u1.first_name, 'mike', msg="Name does not equal Homer")
        # BAD WOLF - only use while developing my test
        print('>>>>', u1.first_name)

    def test_user_create(self):
        u1 = amod.User()
        u1.first_name = 'Harry'
        u1.last_name = 'Potter'
        u1.save()

        u2 = amod.User.objects.get(id=u1.id)
        self.assertEqual(u1.first_name, u2.first_name, msg='Does not save' )