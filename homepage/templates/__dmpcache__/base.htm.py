# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1551423238.0361805
_enable_loop = True
_template_filename = 'C:/Users/niema/Documents/project2/homepage/templates/base.htm'
_template_uri = 'base.htm'
_source_encoding = 'utf-8'
import django_mako_plus
import django.utils.html
_exports = ['base_title', 'title', 'site_icon', 'navbar_items', 'site_left', 'site_center', 'site_content', 'site_right']


from datetime import datetime 

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def base_title():
            return render_base_title(context._locals(__M_locals))
        request = context.get('request', UNDEFINED)
        self = context.get('self', UNDEFINED)
        def navbar_items():
            return render_navbar_items(context._locals(__M_locals))
        def site_icon():
            return render_site_icon(context._locals(__M_locals))
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def site_center():
            return render_site_center(context._locals(__M_locals))
        def site_right():
            return render_site_right(context._locals(__M_locals))
        def site_content():
            return render_site_content(context._locals(__M_locals))
        def title():
            return render_title(context._locals(__M_locals))
        def site_left():
            return render_site_left(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('<!DOCTYPE html>\r\n\r\n\r\n\r\n')
        __M_writer('<html>\r\n    <meta charset="UTF-8">\r\n    <head>\r\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'base_title'):
            context['self'].base_title(**pageargs)
        

        __M_writer('\r\n\r\n')
        __M_writer('        <script src="')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)( STATIC_URL ))
        __M_writer('homepage/media/jquery-3.3.1.js"></script>\r\n        <script src="')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)( STATIC_URL ))
        __M_writer('homepage/media/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>\r\n        <link rel="stylesheet" href="')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)( STATIC_URL ))
        __M_writer('homepage/media/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">\r\n         <script src="')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)( STATIC_URL ))
        __M_writer('homepage/media/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>\r\n        <link rel="icon" type="image/png" href="')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)( STATIC_URL ))
        __M_writer('homepage/media/jedi.png" >\r\n\r\n')
        __M_writer('        <script src="/django_mako_plus/dmp-common.min.js"></script>\r\n        ')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)( django_mako_plus.links(self) ))
        __M_writer('\r\n\r\n    </head>\r\n    <body>\r\n    \r\n <div id="test_base_maintanance_alert" class="alert alert-danger"> Hey there. the server is going down tonight! </div>\r\n\r\n        <header>\r\n         <div class="alert alert-warning" role="alert">\r\n  This is a warning alert—check it out!\r\n</div>\r\n       \r\n')
        __M_writer('            <div id="header">\r\n                ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'site_icon'):
            context['self'].site_icon(**pageargs)
        

        __M_writer('\r\n                \r\n                <div  id="navbar">\r\n                    <ul class="nav justify-content-end">\r\n                        \r\n                        <li class="nav-home">\r\n                            <a class="nav-link ')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)( 'active' if request.dmp.page == 'index' else ''))
        __M_writer('" href="/">Home</a>\r\n                        </li>\r\n                        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'navbar_items'):
            context['self'].navbar_items(**pageargs)
        

        __M_writer('\r\n                        \r\n                        <li class="nav-item dropdown custom_bootstrap">\r\n                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">\r\n                            Welcome, User\r\n                            </a>\r\n                            <div class="dropdown-menu " aria-labelledby="navbarDropdown">\r\n                            <a class="dropdown-item custom_bootstrap" href="#">Profile</a>\r\n')
        if request.user.is_authenticated:
            __M_writer('\r\n                                <li>\r\n                                    <a class="dropdown-item custom_bootstrap " href="/account/logout">Log out</a>\r\n                                </li>\r\n\r\n')
        else:
            __M_writer('                                <li>\r\n                                   <a class="dropdown-item custom_bootstrap " href="/account/login">Login</a> \r\n                                </li>\r\n')
        __M_writer('                       </li>\r\n                        \r\n                    </ul>\r\n                </div>\r\n            </div>\r\n        </header>\r\n\r\n        <main>\r\n            <div id ="site_left">\r\n                ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'site_left'):
            context['self'].site_left(**pageargs)
        

        __M_writer('\r\n            </div>\r\n')
        __M_writer('\r\n            <div id ="site_center">\r\n                ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'site_center'):
            context['self'].site_center(**pageargs)
        

        __M_writer('\r\n            </div>\r\n            ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'site_content'):
            context['self'].site_content(**pageargs)
        

        __M_writer('\r\n\r\n            <div id ="site_right">\r\n                ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'site_right'):
            context['self'].site_right(**pageargs)
        

        __M_writer('\r\n            </div>\r\n        </main>\r\n\r\n        <footer>\r\n        <div id="footer">\r\n            ')
        __M_writer('\r\n            <br>\r\n            <p class="text-center" >&copy; copyright  ')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)( datetime.today().strftime("%Y") ))
        __M_writer('\r\n            \r\n\r\n\r\n            \r\n\r\n            \r\n            </div>\r\n        </footer>\r\n\r\n    </body>\r\n</html>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_base_title(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def base_title():
            return render_base_title(context)
        def title():
            return render_title(context)
        __M_writer = context.writer()
        __M_writer('\r\n            <title>Jedi &mdash; ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'title'):
            context['self'].title(**pageargs)
        

        __M_writer('</title>\r\n        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_title(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def title():
            return render_title(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_site_icon(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def site_icon():
            return render_site_icon(context)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        self = context.get('self', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('<img id="site_icon" src="')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)( STATIC_URL ))
        __M_writer('homepage/media/saber.png" alt="python" />')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_navbar_items(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def navbar_items():
            return render_navbar_items(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_site_left(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def site_left():
            return render_site_left(context)
        __M_writer = context.writer()
        __M_writer('\r\n                    Left\r\n                ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_site_center(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def site_center():
            return render_site_center(context)
        __M_writer = context.writer()
        __M_writer('\r\n                    Center\r\n                ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_site_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def site_content():
            return render_site_content(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_site_right(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def site_right():
            return render_site_right(context)
        __M_writer = context.writer()
        __M_writer('\r\n                    RIght\r\n                ')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:/Users/niema/Documents/project2/homepage/templates/base.htm", "uri": "base.htm", "source_encoding": "utf-8", "line_map": {"18": 138, "20": 0, "44": 2, "45": 12, "50": 17, "51": 20, "52": 20, "53": 20, "54": 21, "55": 21, "56": 22, "57": 22, "58": 23, "59": 23, "60": 24, "61": 24, "62": 27, "63": 28, "64": 28, "65": 70, "70": 71, "71": 77, "72": 77, "77": 79, "78": 87, "79": 88, "80": 95, "81": 96, "82": 103, "87": 114, "88": 121, "93": 125, "98": 127, "103": 132, "104": 138, "105": 140, "106": 140, "112": 15, "120": 15, "125": 16, "131": 16, "142": 71, "150": 71, "151": 71, "152": 71, "158": 79, "169": 112, "175": 112, "181": 123, "187": 123, "193": 127, "204": 130, "210": 130, "216": 210}}
__M_END_METADATA
"""
