# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1550607321.1391835
_enable_loop = True
_template_filename = 'C:/Users/niema/Documents/project2/homepage/templates/index.html'
_template_uri = 'index.html'
_source_encoding = 'utf-8'
import django_mako_plus
import django.utils.html
_exports = ['title', 'navbar_items', 'site_left', 'site_right', 'site_center']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def site_center():
            return render_site_center(context._locals(__M_locals))
        request = context.get('request', UNDEFINED)
        def site_left():
            return render_site_left(context._locals(__M_locals))
        def site_right():
            return render_site_right(context._locals(__M_locals))
        self = context.get('self', UNDEFINED)
        def title():
            return render_title(context._locals(__M_locals))
        def navbar_items():
            return render_navbar_items(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'title'):
            context['self'].title(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'navbar_items'):
            context['self'].navbar_items(**pageargs)
        

        __M_writer('\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'site_left'):
            context['self'].site_left(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'site_right'):
            context['self'].site_right(**pageargs)
        

        __M_writer('\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'site_center'):
            context['self'].site_center(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_title(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def title():
            return render_title(context)
        __M_writer = context.writer()
        __M_writer('Homepage')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_navbar_items(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        request = context.get('request', UNDEFINED)
        def navbar_items():
            return render_navbar_items(context)
        self = context.get('self', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n    <li class="nav-about ')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)( 'active' if request.dmp.page == 'about' else ''))
        __M_writer('">\r\n        <a class="nav-link" href="/homepage/about/">About</a>\r\n')
        __M_writer('    </li>\r\n    <li class="nav-contact ')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)( 'active' if request.dmp.page == 'contact' else ''))
        __M_writer('">\r\n        <a class="nav-link" href="/homepage/contact/">Contact</a>\r\n    </li>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_site_left(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def site_left():
            return render_site_left(context)
        __M_writer = context.writer()
        __M_writer('\r\n    <div class="site_left">\r\n        <p>Morbi eros leo, euismod vel tristique eget, laoreet placerat dui. Donec dapibus pretium lorem id vestibulum. Morbi laoreet non libero ut maximus.</p>\r\n        <p>Donec cursus aliquam elit, et consectetur neque consectetur nec. Cras quam ante, blandit eget justo et, dictum convallis lorem.</p>\r\n        \r\n    </div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_site_right(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def site_right():
            return render_site_right(context)
        __M_writer = context.writer()
        __M_writer('\r\n    <div class="site_right">\r\n        <p>Vivamus nisi enim, tincidunt in iaculis euismod, imperdiet convallis risus.</p>\r\n        <p>Donec urna massa, ornare ac elit eu, mollis facilisis orci.</p>\r\n    </div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_site_center(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def site_center():
            return render_site_center(context)
        __M_writer = context.writer()
        __M_writer('\r\n    <div class="site_center">\r\n    <ul>\r\n    <li>* 2 create a DMP project</li>\r\n    <li>* 2 install Sass (SCSS)</li>\r\n    <li>* 2 switch the `.css` files to `.scss`, and set up DMP to compile them</li>\r\n    <li>* 2 ensure logging is happening in your console</li>\r\n    <li>* 2 download Bootstrap and JQuery to your `homepage/media/` folder</li>\r\n    <li>* 2 all links in your site should use static url: </li>\r\n    </div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:/Users/niema/Documents/project2/homepage/templates/index.html", "uri": "index.html", "source_encoding": "utf-8", "line_map": {"29": 0, "46": 1, "51": 5, "56": 21, "61": 28, "66": 35, "71": 46, "77": 5, "83": 5, "89": 13, "97": 13, "98": 14, "99": 14, "100": 17, "101": 18, "102": 18, "108": 22, "114": 22, "120": 30, "126": 30, "132": 36, "138": 36, "144": 138}}
__M_END_METADATA
"""
